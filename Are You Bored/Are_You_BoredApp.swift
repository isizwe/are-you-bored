import SwiftUI

@main
struct Are_You_BoredApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
