import SwiftUI

struct ContentView: View {
    @State var activity: String = ""
    var body: some View {
        VStack {
            Text(activity)
                .font(.title)
                .padding()
            Button(action: {
                activity = "Do push ups"
            }, label: {
                Text("Press here if Bored")
            })
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
